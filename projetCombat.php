<?php

declare (strict_types=1);

// Tableau de stockage
(array) $PNJs = [];
(array) $PJs = [];



// Variables
(int) $choixPerso = -1;
(int) $totalNotePNJ = 0;
(int) $totalNotePJ = 0;
(int) $choixCombatPJ = 0;
(int) $choixCombatPNJ = 0;
(int) $pointsPJ = 0;
(int) $pointsPNJ = 0;
(int) $numPlayer = 1;
(int) $variationAttaquePJ = 0;
(int) $variationAttaquePNJ = 0;

// Choix nombre joueur
while ($choixPerso < 0){
    print('Avec combien de personnage voulez vous jouer ? ');
    $choixPerso = intval(trim(fgets(STDIN)));
}


///////////////////////// Début saisie /////////////////////////


// Début de boucle choixPerso
for ($i = 0 ; $i < $choixPerso ; $i++){

// Tableau PNJ
(array) $PNJ = [
    'nom' => '',
    'race' => '',
    'profession' => '',
    'arme' => '',
    'bouclier' => '',
    'armure' => '',
    'modAttaque' => 0,
    'modDefense' => 0,
    'armureTotale' => 0,
    'rapidite' => 0,
    'listeSorts' => '',
    'listePotions' => '',
    'equipement' => '',
    'richesses' => 0,
    'vie' => 0,
    'note' => '',
    'endurance' => 0
];

// Tableau PJ
(array) $PJ = [
    'nom' => '',
    'race' => '',
    'profession' => '',
    'arme' => '',
    'bouclier' => '',
    'armure' => '',
    'modAttaque' => 0,
    'modDefense' => 0,
    'armureTotale' => 0,
    'rapidite' => 0,
    'listeSorts' => '',
    'listePotions' => '',
    'equipement' => '',
    'richesses' => 0,
    'vie' => 0,
    'note' => '',
    'endurance' => 0
];

// Variables
(int) $notePNJ = 0;
(int) $notePJ = 0;

// Noms des joueurs
(array) $noms = ["Anderson", "Ashwoon", "Aikin", "Bateman", "Bongard", "Bowers", "Boyd", "Cannon", "Cast", "Deitz", "Dewalt", "Ebner", "Frick", "Hancock", "Haworth", "Hesch", "Hoffman", "Kassing", "Knutson", "Lawless", "Lawicki", "Mccord", "McCormack", "Miller", "Myers", "Nugent", "Ortiz", "Orwig", "Ory", "Paiser", "Pak", "Pettigrew", "Quinn", "Quizoz", "Ramachandran", "Resnick", "Sagar", "Schickowski", "Schiebel", "Sellon", "Severson", "Shaffer", "Solberg", "Soloman", "Sonderling", "Soukup", "Soulis", "Stahl", "Sweeney", "Tandy", "Trebil", "Trusela", "Trussel", "Turco", "Uddin", "Uflan", "Ulrich", "Upson", "Vader", "Vail", "Valente", "Van Zandt", "Vanderpoel", "Ventotla", "Vogal", "Wagle", "Wagner", "Wakefield", "Weinstein", "Weiss", "Woo", "Yang", "Yates", "Yocum", "Zeaser", "Zeller", "Ziegler", "Bauer", "Baxster", "Casal", "Cataldi", "Caswell", "Celedon", "Chambers", "Chapman", "Christensen", "Darnell", "Davidson", "Davis", "DeLorenzo", "Dinkins", "Doran", "Dugelman", "Dugan", "Duffman", "Eastman", "Ferro", "Ferry", "Fletcher", "Fietzer", "Hylan", "Hydinger", "Illingsworth", "Ingram", "Irwin", "Jagtap", "Jenson", "Johnson", "Johnsen", "Jones", "Jurgenson", "Kalleg", "Kaskel", "Keller", "Leisinger", "LePage", "Lewis", "Linde", "Lulloff", "Maki", "Martin", "McGinnis", "Mills", "Moody", "Moore", "Napier", "Nelson", "Norquist", "Nuttle", "Olson", "Ostrander", "Reamer", "Reardon", "Reyes", "Rice", "Ripka", "Roberts", "Rogers", "Root", "Sandstrom", "Sawyer", "Schlicht", "Schmitt", "Schwager", "Schutz", "Schuster", "Tapia", "Thompson", "Tiernan", "Tisler"];

$PNJ['nom'] = $noms[array_rand($noms)];

// Boucle pour éviter les doublons
$PJ['nom'] = $PNJ['nom'];

while ($PJ['nom'] == $PNJ['nom']){
$PJ['nom'] = $noms[array_rand($noms)];
}

// Races des joueurs
(array) $races = ['Samnite', 'Gaulois', 'Thrace', 'Hoplomaque', 'Provocator', 'Secutor', 'Mirmillon', 'Rétiaire'];

$PNJ['race'] = $races[array_rand($races)];
$PJ['race'] = $races[array_rand($races)];

// Professions des joueurs
(array) $professions = ['Esclave', 'Tailleurs de pierres', 'Bucherons', 'Paysan', 'Menuisier', 'Forgeron', 'Souffleur de verre', 'Militaires'];

$PNJ['profession'] = $professions[array_rand($professions)];
$PJ['profession'] = $professions[array_rand($professions)];

// Armes des joueurs
(array) $armes = [
    'armes1' => ['nom' => 'Epée', 'type' => 'arme à 1 main', 'maniabilite' => -10, 'degat' => 45],
    'armes2' => ['nom' => 'Hache', 'type' => 'arme à 1 main', 'maniabilite' => -15, 'degat' => 40],
    'armes3' => ['nom' => 'Lance', 'type' => 'arme à 2 mains', 'maniabilite' => -20, 'degat' => 35],
    'armes4' => ['nom' => 'Flechettes', 'type' => 'arme de lancer', 'maniabilite' => -25, 'degat' => 30],
    'armes5' => ['nom' => 'Dague', 'type' => 'arme de lancer', 'maniabilite' => -30, 'degat' => 25],
    'armes6' => ['nom' => 'Arc', 'type' => 'arme de distance', 'maniabilite' => -35, 'degat' => 20],
    'armes7' => ['nom' => 'Arbalete', 'type' => 'arme de distance', 'maniabilite' => -40, 'degat' => 15],
    'armes8' => ['nom' => 'Griffes', 'type' => 'arme naturelle', 'maniabilite' => -45, 'degat' => 10]
];

$PNJ['arme'] = $armes[array_rand($armes)];
$PJ['arme'] = $armes[array_rand($armes)];

// Boucliers des joueurs
(array) $boucliers = [
    'bouclier1' => ['nom' => 'Bouclier en peau', 'armure' => 15],
    'bouclier2' => ['nom' => 'Bouclier leger', 'armure' => 30],
    'bouclier3' => ['nom' => 'Bouclier en cuir souple', 'armure' => 45],
    'bouclier4' => ['nom' => 'Bouclier de plaque', 'armure' => 60],
    'bouclier5' => ['nom' => 'Bouclier en ecaille de dragon', 'armure' => 80]
];

$PNJ['bouclier'] = $boucliers[array_rand($boucliers)];
$PJ['bouclier'] = $boucliers[array_rand($boucliers)];

// Armures des joueurs
(array) $armures = [
    'armure1' => ['nom' => 'Armure en peau', 'armure' => 15],
    'armure2' => ['nom' => 'Armure legère', 'armure' => 30],
    'armure3' => ['nom' => 'Armure en cuir souple', 'armure' => 45],
    'armure4' => ['nom' => 'Armure de plaque', 'armure' => 60],
    'armure5' => ['nom' => 'Armure en ecaille de dragon', 'armure' => 80]
];

$PNJ['armure'] = $armures[array_rand($armures)];
$PJ['armure'] = $armures[array_rand($armures)];

// Modificateur d'attaque
$PNJ['modAttaque'] = random_int(0, 25);
$PJ['modAttaque'] = random_int(0, 25);

// Modificateur de défense
$PNJ['modDefense'] = random_int(0, 25);
$PJ['modDefense'] = random_int(0, 25);

// Armure totale joueurs
$PNJ['armureTotale'] = $PNJ['armure']['armure'] + $PNJ['bouclier']['armure'];
$PJ['armureTotale'] = $PJ['armure']['armure'] + $PJ['bouclier']['armure'];

// rapidité joueurs
$PNJ['rapidite'] = random_int(5, 25);
$PJ['rapidite'] = random_int(5, 25);

// Sorts des joueurs
(array) $sorts = [
    'sort1' => ['nom' => 'de glace', 'degat' => 15],
    'sort2' => ['nom' => 'de feu', 'degat' => 25],
    'sort3' => ['nom' => 'éléctrique', 'degat' => 30],
    'sort4' => ['nom' => 'de lave', 'degat' => 35],
    'sort5' => ['nom' => 'de mort', 'degat' => 45]
];

$PNJ['listeSorts'] = $sorts[array_rand($sorts)];
$PJ['listeSorts'] = $sorts[array_rand($sorts)];

// Potions des joueurs
(array) $potions = [
    'potion1' => ['nom' => 'niv 1', 'vie' => 10],
    'potion2' => ['nom' => 'niv 2', 'vie' => 20],
    'potion3' => ['nom' => 'niv 3', 'vie' => 30]
];

$PNJ['listePotions'] = $potions[array_rand($potions)];
$PJ['listePotions'] = $potions[array_rand($potions)];

// Equipements des joueurs
(array) $equipements = [
    'equipement1' => ['nom' => "bague de vie", 'vie' => 15],
    'equipement2' => ['nom' => "bracelet de vie", 'vie' => 30],
    'equipement3' => ['nom' => "amulette de vie", 'vie' => 50]
];

$PNJ['equipement'] = $equipements[array_rand($equipements)];
$PJ['equipement'] = $equipements[array_rand($equipements)];

// Vie des joueurs
$PNJ['vie'] = $PNJ['equipement']['vie'] + random_int(80, 100);
$PJ['vie'] = $PJ['equipement']['vie'] + random_int(80, 100);

// Notes des joueurs
$notePNJ = $PNJ['armureTotale'] + $PNJ['vie'];
$notePJ = $PJ['armureTotale'] + $PJ['vie'];

// Total notes joueurs
$totalNotePNJ = $totalNotePNJ + $notePNJ;
$totalNotePJ = $totalNotePJ + $notePJ;

// Note ordinateur
if ($notePNJ < 200){
    $PNJ['note'] = "Personnage moyen";
} elseif ($notePNJ < 240) {
    $PNJ['note'] = "Personnage plutôt bon";
} else {
    $PNJ['note'] = "Personnage légendaire";
}

// Note joueur
if ($notePJ < 200){
    $PJ['note'] = "Personnage moyen";
} elseif ($notePJ < 240) {
    $PJ['note'] = "Personnage plutôt bon";
} else {
    $PJ['note'] = "Personnage légendaire";
}

// Endurance joueurs
$PNJ['endurance'] = $PNJ['armureTotale'] + $PNJ['vie'];
$PJ['endurance'] = $PJ['armureTotale'] + $PJ['vie'];

// Stockage dans les tableaux
$PNJs[] = $PNJ;
$PJs[] = $PJ;

// Fin de boucle $choixPerso
}

///////////////////////////////////////////////////////////////////////////////////
print_r($PNJs);



///////////////////////// Fin saisie /////////////////////////

///////////////////////// Début récapitulatif /////////////////////////

// Boucle de séléction des personnages
for($i = 0 ; $i < $choixPerso ; $i++){

    $numPlayer = $numPlayer + $i;

// Print info personnage
print(PHP_EOL . '////////////////Votre personnage N°'.$numPlayer.'////////////////' . PHP_EOL);
print(PHP_EOL . '------------------Informations------------------' . PHP_EOL);
printf(PHP_EOL . 'Nom : %s' . PHP_EOL . 'Race : %s' . PHP_EOL . 'Profession : %s' . PHP_EOL . 'Endurance : %d' . PHP_EOL,
    $PJs[$i]['nom'],
    $PJs[$i]['race'],
    $PJs[$i]['profession'],
    $PJs[$i]['endurance']
);

print(PHP_EOL . '------------------Equipements------------------' . PHP_EOL);
printf(PHP_EOL . 'Armes : %s (%d points de dégâts)' . PHP_EOL . 'Bouclier : %s (%d points d\'armures)' . PHP_EOL . 'Armure : %s (%d points d\'armure)' . PHP_EOL . 'Sort : Sort %s (%d points de dégâts)' . PHP_EOL . 'Potion : Potion %s (%d points de vie)' . PHP_EOL,
    $PJs[$i]['arme']['nom'],
    $PJs[$i]['arme']['degat'],
    $PJs[$i]['bouclier']['nom'],
    $PJs[$i]['bouclier']['armure'],
    $PJs[$i]['armure']['nom'],
    $PJs[$i]['armure']['armure'],
    $PJs[$i]['listeSorts']['nom'],
    $PJs[$i]['listeSorts']['degat'],
    $PJs[$i]['listePotions']['nom'],
    $PJs[$i]['listePotions']['vie']
);

print(PHP_EOL . '**************' . $PJs[$i]['note'] . '**************' . PHP_EOL);

// Print info ordinateur
print(PHP_EOL . '////////////////Personnage ordi N°'.$numPlayer.'////////////////' . PHP_EOL);
print(PHP_EOL . '------------------Informations------------------' . PHP_EOL);
printf(PHP_EOL . 'Nom : %s' . PHP_EOL . 'Race : %s' . PHP_EOL . 'Profession : %s' . PHP_EOL . 'Endurance : %d' . PHP_EOL,
    $PNJs[$i]['nom'],
    $PNJs[$i]['race'],
    $PNJs[$i]['profession'],
    $PNJs[$i]['endurance']
);

print(PHP_EOL . '------------------Equipements------------------' . PHP_EOL);
printf(PHP_EOL . 'Armes : %s (%d points de dégâts)' . PHP_EOL . 'Bouclier : %s (%d points d\'armures)' . PHP_EOL . 'Armure : %s (%d points d\'armure)' . PHP_EOL . 'Sort : Sort %s (%d points de dégâts)' . PHP_EOL . 'Potion : Potion %s (%d points de vie)' . PHP_EOL,
    $PNJs[$i]['arme']['nom'],
    $PNJs[$i]['arme']['degat'],
    $PNJs[$i]['bouclier']['nom'],
    $PNJs[$i]['bouclier']['armure'],
    $PNJs[$i]['armure']['nom'],
    $PNJs[$i]['armure']['armure'],
    $PNJs[$i]['listeSorts']['nom'],
    $PNJs[$i]['listeSorts']['degat'],
    $PNJs[$i]['listePotions']['nom'],
    $PNJs[$i]['listePotions']['vie']
);

print(PHP_EOL . '**************' . $PNJs[$i]['note'] . '**************' . PHP_EOL);


///////////////////////// Fin récapitulatif /////////////////////////


    ///////////////////////// Début combat /////////////////////////

    while ($PNJs[$i]['endurance'] > 0 && $PJs[$i]['endurance'] > 0){

        ///////////////////PJ///////////////////

        $choixCombatPJ = 0;

        while ($choixCombatPJ <= 0 || $choixCombatPJ > 3) {
            print(PHP_EOL . '///////////////////Votre tour///////////////////' . PHP_EOL);

            print(PHP_EOL . 'Choisissez votre action ?' . PHP_EOL);

            printf(PHP_EOL . '1 = Attaque avec %s (%d de dégâts)' . PHP_EOL . '2 = sort %s (%d de dégâts)' . PHP_EOL . '3 = potion %s (%d de vie)' . PHP_EOL,
                $PJs[$i]['arme']['nom'],
                $PJs[$i]['arme']['degat'],
                $PJs[$i]['listeSorts']['nom'],
                $PJs[$i]['listeSorts']['degat'],
                $PJs[$i]['listePotions']['nom'],
                $PJs[$i]['listePotions']['vie']
            );

            print(PHP_EOL . 'Votre choix : ');
            $choixCombatPJ = intval(trim(fgets(STDIN)));
        }
        if ($choixCombatPJ == 1) {
            $variationAttaquePJ = $PJs[$i]['modAttaque'] - $PNJs[$i]['modDefense'] + $PJs[$i]['rapidite'] - random_int(0, 25);
            if($variationAttaquePJ > 0){
                $PNJs[$i]['endurance'] = $PNJs[$i]['endurance'] - $PJs[$i]['arme']['degat'];
            }
        } elseif ($choixCombatPJ == 2) {
            $variationAttaquePJ = $PJs[$i]['modAttaque'] - $PNJs[$i]['modDefense'] + $PJs[$i]['rapidite'] - random_int(0, 25);
            if($variationAttaquePJ > 0){
                $PNJs[$i]['endurance'] = $PNJs[$i]['endurance'] - $PJs[$i]['listeSorts']['degat'];
            } else {
                $PJs[$i]['endurance'] = $PJs[$i]['endurance'] - $PJs[$i]['listeSorts']['degat'] / 2;
            }
        } else {
            $PJs[$i]['endurance'] = $PJs[$i]['endurance'] + $PJs[$i]['listePotions']['vie'];

        }

        ///////////////////PNJ///////////////////

        $choixCombatPNJ = 0;

        while ($choixCombatPNJ <= 0 || $choixCombatPNJ > 3) {
            print(PHP_EOL);
            $choixCombatPNJ = random_int(1, 3);
        }

        if ($choixCombatPNJ == 1) {
            $variationAttaquePNJ = $PNJs[$i]['modAttaque'] - $PJs[$i]['modDefense'] + $PNJs[$i]['rapidite'] - random_int(0, 25);
            if($variationAttaquePNJ > 0){
                $PJs[$i]['endurance'] = $PJs[$i]['endurance'] - $PNJs[$i]['arme']['degat'];
            }
        } elseif ($choixCombatPNJ == 2) {
            $variationAttaquePNJ = $PNJs[$i]['modAttaque'] - $PJs[$i]['modDefense'] + $PNJs[$i]['rapidite'] - random_int(0, 25);
            if($variationAttaquePNJ > 0){
                $PJs[$i]['endurance'] = $PJs[$i]['endurance'] - $PNJs[$i]['listeSorts']['degat'];
            } else {
                $PNJs[$i]['endurance'] = $PNJs[$i]['endurance'] - $PNJs[$i]['listeSorts']['degat'] / 2;
            }
        } else {
            $PNJs[$i]['endurance'] = $PNJs[$i]['endurance'] + $PNJs[$i]['listePotions']['vie'];

        }
        print(PHP_EOL . '///////////////////Récapitulatif///////////////////' . PHP_EOL);

        ///////////////////PJ///////////////////

        if ($choixCombatPJ == 1) {
            $variationAttaquePJ = $PJs[$i]['modAttaque'] - $PNJs[$i]['modDefense'] + $PJs[$i]['rapidite'] - random_int(0, 25);
            if($variationAttaquePJ > 0){
                printf(PHP_EOL . '%s attaque avec l\'arme (%s) et inflige %d points de dégâts à %s' . PHP_EOL,
                    $PJs[$i]['nom'],
                    $PJs[$i]['arme']['nom'],
                    $PJs[$i]['arme']['degat'],
                    $PNJs[$i]['nom']
            );

            printf(PHP_EOL . '%s a maintenant %d points d\'endurance' . PHP_EOL,
                $PNJs[$i]['nom'],
                $PNJs[$i]['endurance']
            );
            } else {
                print('L\'attaque de ' . $PJs[$i]['nom'] . ' a échoué !' . PHP_EOL);
            }

        } elseif ($choixCombatPJ == 2) {
            $variationAttaquePJ = $PJs[$i]['modAttaque'] - $PNJs[$i]['modDefense'] + $PJs[$i]['rapidite'] - random_int(0, 25);
            if($variationAttaquePJ > 0){
                printf(PHP_EOL . '%s attaque avec le sort %s et inflige %d points de dégâts à %s' . PHP_EOL,
                    $PJs[$i]['nom'],
                    $PJs[$i]['listeSorts']['nom'],
                    $PJs[$i]['listeSorts']['degat'],
                    $PNJs[$i]['nom']
            );

            printf(PHP_EOL . '%s a maintenant %d points d\'endurance' . PHP_EOL,
                $PNJs[$i]['nom'],
                $PNJs[$i]['endurance']
            );
            } else {
                print('L\'attaque de sort de ' . $PJs[$i]['nom'] . ' a échoué !' . PHP_EOL);
                printf('%s reçoit quelques dégâts pour l\'avoir lancé à ces pieds' . PHP_EOL,
                    $PJs[$i]['nom']
                );
            }

        } else {
            printf(PHP_EOL . '%s utilise la potion %s et gagne %d points de vie' . PHP_EOL,
                $PJs[$i]['nom'],
                $PJs[$i]['listePotions']['nom'],
                $PJs[$i]['listePotions']['vie']
            );

            printf(PHP_EOL . '%s a maintenant %d points d\endurance' . PHP_EOL,
                $PJs[$i]['nom'],
                $PJs[$i]['endurance']
            );

        }

        ///////////////////PNJ///////////////////

        if ($choixCombatPNJ == 1) {
            $variationAttaquePNJ = $PNJs[$i]['modAttaque'] - $PJs[$i]['modDefense'] + $PNJs[$i]['rapidite'] - random_int(0, 25);
            if($variationAttaquePNJ > 0){
                printf(PHP_EOL . '%s attaque avec l\'arme (%s) et inflige %d points de dégâts à %s' . PHP_EOL,
                    $PNJs[$i]['nom'],
                    $PNJs[$i]['arme']['nom'],
                    $PNJs[$i]['arme']['degat'],
                    $PJs[$i]['nom']
                );

                printf(PHP_EOL . '%s a maintenant %d points d\'endurance' . PHP_EOL,
                $PJs[$i]['nom'],
                $PJs[$i]['endurance']
                );
            } else {
                print('L\'attaque de ' . $PNJs[$i]['nom'] . ' a échoué !' . PHP_EOL);
            }

        } elseif ($choixCombatPNJ == 2) {
            $variationAttaquePNJ = $PJs[$i]['modAttaque'] - $PNJs[$i]['modDefense'] + $PJs[$i]['rapidite'] - random_int(0, 25);
            if($variationAttaquePNJ > 0){
            printf(PHP_EOL . '%s attaque avec le sort %s et inflige %d points de dégâts à %s' . PHP_EOL,
                $PNJs[$i]['nom'],
                $PNJs[$i]['listeSorts']['nom'],
                $PNJs[$i]['listeSorts']['degat'],
                $PJs[$i]['nom']
            );

            printf(PHP_EOL . '%s a maintenant %d points d\'endurance' . PHP_EOL,
                $PJs[$i]['nom'],
                $PJs[$i]['endurance']
            );

            } else {
                print('L\'attaque de sort de ' . $PNJs[$i]['nom'] . ' a échoué !' . PHP_EOL);
                printf('%s reçoit quelques dégâts pour l\'avoir lancé à ces pieds' . PHP_EOL,
                    $PNJs[$i]['nom']
                );

                printf(PHP_EOL . '%s a maintenant %d points d\'endurance' . PHP_EOL,
                    $PNJs[$i]['nom'],
                    $PNJs[$i]['endurance']
                );
            }

        } else {
            printf(PHP_EOL . '%s utilise la potion %s et gagne %d points de vie' . PHP_EOL,
                $PNJs[$i]['nom'],
                $PNJs[$i]['listePotions']['nom'],
                $PNJs[$i]['listePotions']['vie']
            );

                printf(PHP_EOL . '%s a maintenant %d points d\endurance' . PHP_EOL,
                $PNJs[$i]['nom'],
                $PNJs[$i]['endurance']
            );

        }
        printf(PHP_EOL . PHP_EOL . '(Joueur) %s %d <<<>>> %d %s (Ordinateur)' . PHP_EOL,
            $PJs[$i]['nom'],
            $PJs[$i]['endurance'],
            $PNJs[$i]['endurance'],
            $PNJs[$i]['nom']
        );

    }

    if ($PJs[$i]['endurance'] > 0){
        print(PHP_EOL . 'Vous avez gagnée le combat N°' . $numPlayer . ' :)' . PHP_EOL);
        $pointsPJ++;
    } else {
        print(PHP_EOL . 'Vous avez perdu le combat N°' . $numPlayer . ' :(' . PHP_EOL);
        $pointsPNJ++;
    }

}


    ///////////////////////// Fin combat /////////////////////////

print(PHP_EOL . '///////////////////////// Fin de partie /////////////////////////' . PHP_EOL);

if ($pointsPJ > $pointsPNJ){
    print(PHP_EOL . 'Vous avez gagnée la partie :D' . PHP_EOL);
} else {
    print(PHP_EOL . 'Vous avez perdu la partie :S' . PHP_EOL);
}